import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class GroupAnagramsTest {

    GroupAnagrams groupAnagrams = new GroupAnagrams();

    @Test
    public void test1() {
        String[] input = Arrays.asList("eat","tea","tan","ate","nat","bat").toArray(new String[0]);

        Assertions.assertEquals(Arrays.asList(
                Arrays.asList("eat", "tea", "ate"),
                Arrays.asList("tan", "nat"),
                Arrays.asList("bat")
        ), groupAnagrams.groupAnagrams(input));
    }
}
