import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class TwoSumTest {

    private TwoSum twoSum = new TwoSum();

    @Test
    void twoSetTest() {
        int[] nums = {2,7,11,15};
        int target = 9;

        int[] expected = {0,1};

        Assertions.assertArrayEquals(expected, twoSum.twoSum(nums, target));
    }

    @Test
    void twoSetTest2() {
        int[] nums = {3,2,4};
        int target = 6;

        int[] expected = {1,2};

        Assertions.assertArrayEquals(expected, twoSum.twoSum(nums, target));
    }
}
