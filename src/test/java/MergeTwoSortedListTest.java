import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class MergeTwoSortedListTest {

    MergeTwoSortedList mergeTwoSortedList = new MergeTwoSortedList();

    @Test
    void mergeTwoSortedList() {
        MergeTwoSortedList.ListNode first = createListNode(new int[]{1,2,4});
        MergeTwoSortedList.ListNode second = createListNode(new int[]{1,3,4});

        int[] expected = new int[]{1,1,2,3,4,4};
        MergeTwoSortedList.ListNode listNode = mergeTwoSortedList.mergeTwoLists(first, second);
        System.out.println("1");
        //Assertions.assertArrayEquals(expected, output(mergeTwoSortedList.mergeTwoLists(first, second)));
    }

    public MergeTwoSortedList.ListNode createListNode(int[] array) {
        MergeTwoSortedList.ListNode prevNode = null;
        MergeTwoSortedList.ListNode listNode = null;
        for (int i = 0; i < array.length; i++) {
            listNode = new MergeTwoSortedList.ListNode(array[array.length-1-i], prevNode);
            prevNode = listNode;
        }
        return listNode;
    }

    public int[] output(MergeTwoSortedList.ListNode listNode) {
        List<Integer> list = new ArrayList<>();
        MergeTwoSortedList.ListNode cur = listNode;
        if (cur != null) {
            list.add(cur.val);
            cur = cur.next;
        }
        return list.stream()
                .mapToInt(Integer::intValue)
                .toArray();
    }

}
