package codility.opera;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Task2OperaTest {

    private Task2Opera task2Opera = new Task2Opera();

    @Test
    void test1() {
        int[][] A = new int[][]{
                {7, 3, 8},
                {7, 6, 5},
                {4, 9, 5}
        };

        Assertions.assertEquals(3, task2Opera.solution(A));
    }

    @Test
    void test2() {
        int[][] A = new int[][]{
                {4, 3, 4, 5, 3},
                {2, 7, 3, 8, 4},
                {1, 7, 6, 5, 2},
                {8, 4, 9, 5, 5}
        };

        Assertions.assertEquals(3, task2Opera.solution(A));
    }

    @Test
    void test3() {
        int[][] A = new int[][]{
                {2, 2, 1, 1},
                {2, 2, 2, 2},
                {1, 2, 2, 2}
        };

        Assertions.assertEquals(2, task2Opera.solution(A));
    }

    @Test
    void test4() {
        int[][] A = new int[][]{
                {7, 2, 4},
                {2, 7, 6},
                {9, 5, 1},
                {4, 3, 8},
                {3, 5, 4}
        };

        Assertions.assertEquals(3, task2Opera.solution(A));
    }

    @Test
    void test5() {
        int[][] A = new int[][]{
                {1, 0, 7, 2, 4, 9, 9},
                {1, 0, 2, 7, 6, 9, 9},
                {1, 0, 9, 5, 1, 9, 9},
                {1, 0, 4, 3, 8, 9, 9},
                {1, 0, 3, 5, 4, 9, 9}
        };

        Assertions.assertEquals(3, task2Opera.solution(A));
    }

    @Test
    void test6() {
        int[][] A = new int[][]{
                {-1, 1, 0, 3, 5, 4, 9, 9},
                {-1, 1, 0, 7, 2, 4, 9, 9},
                {-1, 1, 0, 2, 7, 6, 9, 9},
                {-1, 1, 0, 9, 5, 1, 9, 9},
                {-1, 1, 0, 4, 3, 8, 9, 9},
                {-1, 1, 0, 3, 5, 4, 9, 9},
                {-1, 1, 0, 3, 5, 4, 9, 9}
        };

        Assertions.assertEquals(3, task2Opera.solution(A));
    }
}