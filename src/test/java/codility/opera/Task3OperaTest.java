package codility.opera;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Task3OperaTest {

    private Task3Opera task3Opera = new Task3Opera();

    @Test
    void test1() {
        int[] A = new int[]{2, 6, 8, 5};

        Assertions.assertEquals(3, task3Opera.solution(A));
        Assertions.assertEquals(3, task3Opera.solutionFirst(A));

    }

    @Test
    void test2() {
        int[] A = new int[]{1, 5, 5, 2, 6};

        Assertions.assertEquals(4, task3Opera.solution(A));
        Assertions.assertEquals(4, task3Opera.solutionFirst(A));
    }

    @Test
    void test3() {
        int[] A = new int[]{0, 1};

        Assertions.assertEquals(2, task3Opera.solution(A));
        Assertions.assertEquals(2, task3Opera.solutionFirst(A));
    }

    @Test
    void test4() {
        int[] A = new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0};

        Assertions.assertEquals(11, task3Opera.solution(A));
        Assertions.assertEquals(11, task3Opera.solutionFirst(A));
    }

    @Test
    void test5() {
        int[] A = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        Assertions.assertEquals(11, task3Opera.solution(A));
        Assertions.assertEquals(11, task3Opera.solutionFirst(A));
    }

    @Test
    void test6() {
        int[] A = new int[]{1, 1, 1, 2, 2, 1, 1, 1, 1, 2, 2};

        Assertions.assertEquals(8, task3Opera.solution(A));
        Assertions.assertEquals(8, task3Opera.solutionFirst(A));
    }

    @Test
    void test7() {
        int[] A = new int[]{2, 2, 1, 1, 1, 1, 2, 2, 1, 1, 1, 2, 2};

        Assertions.assertEquals(8, task3Opera.solution(A));
        Assertions.assertEquals(8, task3Opera.solutionFirst(A));
    }
}