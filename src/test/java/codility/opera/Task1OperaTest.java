package codility.opera;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Task1OperaTest {

    private Task1Opera task1Opera = new Task1Opera();

    @Test
    void test1() {
        String s = "SSMLSML";

        Assertions.assertEquals("SSSMMLL", task1Opera.solution(s));
    }

    @Test
    void test2() {
        String s = "SSSS";

        Assertions.assertEquals("SSSS", task1Opera.solution(s));
    }

    @Test
    void test3() {
        String s = "LLMMSSSS";

        Assertions.assertEquals("SSSSMMLL", task1Opera.solution(s));
    }

    @Test
    void test4() {
        String s = "LSSSS";

        Assertions.assertEquals("SSSSL", task1Opera.solution(s));
    }
}