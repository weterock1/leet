package codility.eurora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class Task1TestEurora {

    Task1Eurora task1 = new Task1Eurora();

    @Test
    void testTrue1() {
        int[] a = {3,1,2};
        int[] b = {2,3,1};

        Assertions.assertTrue(task1.solution(a, b));
    }

    @Test
    void testTrue2() {
        int[] a = {1,3,2,4};
        int[] b = {4,1,3,2};

        Assertions.assertTrue(task1.solution(a, b));
    }

    @Test
    void testFalse0() {
        int[] a = {1,2,3,4};
        int[] b = {2,1,4,3};

        Assertions.assertFalse(task1.solution(a, b));
    }

    @Test
    void testFalse1() {
        int[] a = {1,2,1};
        int[] b = {2,3,3};

        Assertions.assertFalse(task1.solution(a, b));
    }

    @Test
    void testFalse2() {
        int[] a = {1,2,3,4};
        int[] b = {2,1,4,4};

        Assertions.assertFalse(task1.solution(a, b));
    }

    @Test
    void testFalse4() {
        int[] a = {1,2,2,3,3};
        int[] b = {2,3,3,4,5};

        Assertions.assertFalse(task1.solution(a, b));
    }
}