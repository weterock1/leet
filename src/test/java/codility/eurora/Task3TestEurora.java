package codility.eurora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class Task3TestEurora {

    Task3Eurora task3 = new Task3Eurora();

    @Test
    void test1() {
        int[] rolls = {3,2,4,3};
        int f = 2;
        int m = 4;

        Assertions.assertArrayEquals(new int[]{6,6}, task3.solution(rolls, f, m));
    }

    @Test
    void test2() {
        int[] rolls = {4,4,4,4};
        int f = 8;
        int m = 5;

        Assertions.assertArrayEquals(new int[]{6,6,6,6,5,5,5,5}, task3.solution(rolls, f, m));
    }

    @Test
    void test3() {
        int[] rolls = {1,1,1,1};
        int f = 8;
        int m = 1;

        Assertions.assertArrayEquals(new int[]{1,1,1,1,1,1,1,1}, task3.solution(rolls, f, m));
    }

    @Test
    void test4() {
        int[] rolls = {6,6,6,6};
        int f = 8;
        int m = 6;

        Assertions.assertArrayEquals(new int[]{6,6,6,6,6,6,6,6}, task3.solution(rolls, f, m));
    }

    @Test
    void test5() {
        int[] rolls = {1,1,1,2};
        int f = 8;
        int m = 1;

        Assertions.assertArrayEquals(new int[]{0}, task3.solution(rolls, f, m));
    }

    @Test
    void test6() {
        int[] rolls = {6,6,6,5};
        int f = 8;
        int m = 6;

        Assertions.assertArrayEquals(new int[]{0}, task3.solution(rolls, f, m));
    }

    @Test
    void test7() {
        int[] rolls = {1,5,6};
        int f = 4;
        int m = 3;

        Assertions.assertArrayEquals(new int[]{0}, task3.solution(rolls, f, m));
    }
}