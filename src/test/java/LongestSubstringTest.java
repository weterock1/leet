import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestSubstringTest {

    private LongestSubstring longestSubstring = new LongestSubstring();

    @Test
    void test1() {
        String s = "abcabcbb";
        Assertions.assertEquals(3, longestSubstring.lengthOfLongestSubstring(s));
    }

    @Test
    void test2() {
        String s = "bbbbb";
        Assertions.assertEquals(1, longestSubstring.lengthOfLongestSubstring(s));
    }

    @Test
    void test3() {
        String s = "pwwkew";
        Assertions.assertEquals(3, longestSubstring.lengthOfLongestSubstring(s));
    }

    @Test
    void test4() {
        String s = " ";
        Assertions.assertEquals(1, longestSubstring.lengthOfLongestSubstring(s));
    }

    @Test
    void test5() {
        String s = "abba";
        Assertions.assertEquals(2, longestSubstring.lengthOfLongestSubstring(s));
    }

    @Test
    void test1_2() {
        String s = "abcabcbb";
        Assertions.assertEquals(3, longestSubstring.lengthOfLongestSubstring2(s));
    }

    @Test
    void test2_2() {
        String s = "bbbbb";
        Assertions.assertEquals(1, longestSubstring.lengthOfLongestSubstring2(s));
    }

    @Test
    void test3_2() {
        String s = "pwwkew";
        Assertions.assertEquals(3, longestSubstring.lengthOfLongestSubstring2(s));
    }

    @Test
    void test4_2() {
        String s = " ";
        Assertions.assertEquals(1, longestSubstring.lengthOfLongestSubstring2(s));
    }

    @Test
    void test5_2() {
        String s = "abba";
        Assertions.assertEquals(2, longestSubstring.lengthOfLongestSubstring2(s));
    }
}