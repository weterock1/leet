import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class IntersectionTest {

    private Intersection intersection = new Intersection();

    @Test
    void testIntesection() {
        int[] nums1 = {4,9,5};
        int[] nums2 = {9,4,9,8,4};
        int[] expected = {4,9};

        Assertions.assertArrayEquals(expected, intersection.getintersection(nums1, nums2));
    }
}
