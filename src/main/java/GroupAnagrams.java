import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupAnagrams {

    public List<List<String>> groupAnagrams(String[] strs) {

        // key - digit key
        // value
        Map<String, List<String>> map = new HashMap<>();

        int constant = 97;

        for (int i = 0; i < strs.length; i++) {
            int[] letters = new int[26];
            String str = strs[i];
            for (int i1 = 0; i1 < str.length(); i1++) {
                letters[str.charAt(i1) - constant] = letters[str.charAt(i1) - constant] + 1;
            }
            map.compute(Arrays.toString(letters), (key, value) -> {
               if (value == null) {
                   List<String> list = new ArrayList<>();
                   list.add(str);
                   return list;
               } else {
                   value.add(str);
                   return value;
               }
            });
        }
        return new ArrayList<>(map.values());
    }

}
