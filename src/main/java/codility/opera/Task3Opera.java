package codility.opera;

public class Task3Opera {

    public int solution(int[] blocks) {
        int max = -1;
        int index = 0;

        boolean isRightFrog = true;
        int expand = 0;
        while (index < blocks.length - 1) {
            if (isRightFrog) {
                expand += expandRight(blocks, index);
                if (expand + 1 > max) {
                    max = expand + 1;
                }
                index += expand;
                isRightFrog = false;
            } else {
                index = minimizeIndex(blocks, index);
                expand = expandRightWithMinimize(blocks, index);
                index += expand;
                isRightFrog = true;
            }
        }
        if (expand + 1 > max) {
            max = expand + 1;
        }
        return max;
    }

    private int minimizeIndex(int[] blocks, int index) {
        int i = 0;
        for (i = index; i > 0; i--) {
            if (blocks[i] != blocks[i-1]) {
                break;
            }
        }

        return i;
    }

    public int solutionFirst(int[] blocks) {
        int max = -1;
        for (int i = 0; i < blocks.length; i++) {
            int i1 = expandRight(blocks, i);
            int i2 = expandLeft(blocks, i);
            int distance = i1 + i2;
            if (max < distance) {
                max = distance;
            }
        }
        return max + 1;
    }

    private int expandLeft(int[] blocks, int from) {
        int cur = blocks[from];
        int i = 0;
        for (i = from; i > 0; i--) {
            if (blocks[i - 1] < blocks[i]) {
                break;
            }
        }
        return from - i;
    }

    private int expandRight(int[] blocks, int from) {
        int cur = blocks[from];
        int i = 0;
        for (i = from; i < blocks.length - 1; i++) {
            if (blocks[i + 1] < blocks[i]) {
                break;
            }
        }
        return i - from;
    }

    private int expandRightWithMinimize(int[] blocks, int from) {
        int cur = blocks[from];
        int i = 0;
        for (i = from; i < blocks.length - 1; i++) {
            if (blocks[i + 1] > blocks[i]) {
                break;
            }
        }
        return i - from;
    }
}
