package codility.opera;

public class Task1Opera {
    public String solution(String T) {
        int[] smlCounter = new int[3];

        for (int i = 0; i < T.length(); i++) {
            char c = T.charAt(i);
            switch (c) {
                case 'S':
                    smlCounter[0]++;
                    break;
                case 'M':
                    smlCounter[1]++;
                    break;
                case 'L':
                    smlCounter[2]++;
                    break;
                default:
                    throw new IllegalStateException("Incorrect input string");
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < smlCounter[0]; i++) {
            sb.append("S");
        }
        for (int i = 0; i < smlCounter[1]; i++) {
            sb.append("M");
        }
        for (int i = 0; i < smlCounter[2]; i++) {
            sb.append("L");
        }
        return sb.toString();
    }
}
