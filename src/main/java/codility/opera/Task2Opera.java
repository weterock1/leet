package codility.opera;

public class Task2Opera {
    public int solution(int[][] A) {
        int rowsCount = A.length;
        int columnsCount = A[0].length;

        int minOfCount = Math.min(rowsCount, columnsCount);
        if (minOfCount == 1) {
            return 1;
        }


        for (int i = minOfCount; i > 1; i--) {
            int xPadding = rowsCount - i;
            int yPadding = columnsCount - i;

            for (int j = 0; j <= xPadding; j++) {
                for (int k = 0; k <= yPadding; k++) {
                    if (isMagic(A, j, k, i)) {
                        return i;
                    }
                }
            }
        }
        return 1;
    }

    private boolean isMagic(int[][] srcMatrix, int fromX, int fromY, int sizeN) {
        Integer firstSum = null;
        // check rows
        int sum = 0;
        for (int i = fromX; i < fromX + sizeN; i++) {
            sum = 0;
            for (int j = fromY; j < fromY + sizeN; j++) {
                sum += srcMatrix[i][j];
            }
            if (firstSum == null) {
                firstSum = sum;
            } else {
                if (!firstSum.equals(sum)) {
                    return false;
                }
            }
        }
        // check columns
        for (int j = fromY; j < fromY + sizeN; j++) {
            sum = 0;
            for (int i = fromX; i < fromX + sizeN; i++) {
                sum += srcMatrix[i][j];
            }
            if (!firstSum.equals(sum)) {
                return false;
            }
        }
        // main diagonal
        sum = 0;
        for (int i = 0; i < sizeN; i++) {
            sum += srcMatrix[fromX+i][fromY+i];
        }
        if (!firstSum.equals(sum)) {
            return false;
        }
        // submain diagonal
        sum = 0;
        for (int i = 0; i < sizeN; i++) {
            sum += srcMatrix[fromX+sizeN-i-1][fromY+i];
        }
        if (!firstSum.equals(sum)) {
            return false;
        }
        return true;
    }
}
