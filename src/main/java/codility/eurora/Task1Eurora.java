package codility.eurora;

import java.util.HashMap;
import java.util.Map;

public class Task1Eurora {

    public boolean solution(int[] A, int[] B) {

        if (A.length == 0) {
            return false;
        }

        Map<Integer, Integer> mapGraph = new HashMap<>();

        for (int i = 0; i < A.length; i++) {
            mapGraph.put(A[i], B[i]);
        }


        int firstElement = A[0];

        if (mapGraph.size() < A.length) {
            // 2 arrows from 1 point
            return false;
        }

        int iterator = 0;
        Integer nextEl = firstElement;
        while (true) {
            nextEl = mapGraph.get(nextEl);
            iterator++;
            if (nextEl == null || nextEl.equals(firstElement)) {
                break;
            }
            if (iterator == A.length) {
                return false;
            }
        }
        return iterator == A.length;
    }
}
