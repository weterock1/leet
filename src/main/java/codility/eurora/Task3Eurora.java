package codility.eurora;

public class Task3Eurora {

    public int[] solution(int[] A, int F, int M) {
        int[] unsuccessfulResult = {0};
        if (F == 0) {
            return unsuccessfulResult;
        }
        // write your code in Java SE 8
        int[] result = new int[F];


        int rememberedLength = A.length;
        int inputSum = 0;
        int correctSum = M * (F + rememberedLength);

        boolean allTheOnes = true;
        boolean allTheSix = true;

        for (int i = 0; i < A.length; i++) {
            if (A[i] != 1) {
                allTheOnes = false;
            }
            if (A[i] != 6) {
                allTheSix = false;
            }
            inputSum += A[i];
        }

        if (M == 1) {
            if (allTheOnes == true) {
                for (int i = 0; i < result.length; i++) {
                    result[i] = 1;
                }
                return result;
            } else {
                return unsuccessfulResult;
            }
        }

        if (M == 6) {
            if (allTheSix == true) {
                for (int i = 0; i < result.length; i++) {
                    result[i] = 6;
                }
                return result;
            } else {
                return unsuccessfulResult;
            }
        }

        int diff = correctSum - inputSum;
        if (diff <= 0 ||
                diff < F ||
                diff > 6*F) {
            return unsuccessfulResult;
        }
        fillArray(result, diff);
        return result;
    }

    private void fillArray(int[] array, int sum) {
        int unit = sum / array.length;
        int fraction = sum % array.length;

        for (int i = 0; i < array.length; i++) {
            if (fraction > 0) {
                array[i] = unit+1;
                fraction--;
            } else {
                array[i] = unit;
            }
        }
    }
}
