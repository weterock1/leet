import java.util.ArrayList;
import java.util.List;

// Thread unsafe
public class Intersection {

    public int[] getintersection(int[] num1, int[] num2) {
        int[] place = new int[1001];
        int[] second = new int[1001];

        for (int i = 0; i < num1.length; i++) {
            place[num1[i]] = place[num1[i]] + 1;
        }
        int j = 0;
        for (int i = 0; i < num2.length; i++) {
            if (place[num2[i]] > 0) {
                j = j+1;
                place[num2[i]] = place[num2[i]] - 1;
                second[num2[i]] = second[num2[i]] + 1;
            }
        }
        int[] result = new int[j];
        int resultIndex = 0;
        for (int i = 0; i < second.length; i++) {
            if (second[i] > 0) {
                for (int i1 = 0; i1 < second[i]; i1++) {
                    result[resultIndex] = i;
                    resultIndex ++;
                }
            }
        }
        return result;
    }
}
