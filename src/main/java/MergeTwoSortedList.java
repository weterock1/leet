public class MergeTwoSortedList {

    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {

        ListNode start = null;
        ListNode el = null;
        if (list1 == null) {
            return list2;
        } else if (list2 == null){
            return list1;
        }
        if (list1.val < list2.val) {
            el = list1;
            list1 = list1.next;
        } else {
            el = list2;
            list2 = list2.next;
        }
        start = el;
        while (true) {
            if (list1 == null && list2 == null) {
                break;
            }
            if (list1 == null) {
                el.next = list2;
                list2 = list2.next;
                el = el.next;
                continue;
            }
            if (list2 == null) {
                el.next = list1;
                list1 = list1.next;
                el = el.next;
                continue;
            }
            if (list1.val < list2.val) {
                el.next = list1;
                list1 = list1.next;
                el = el.next;
            } else {
                el.next = list2;
                list2 = list2.next;
                el = el.next;
            }

        }
        return start;
    }

    public static class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
   }

}
