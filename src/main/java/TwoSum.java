import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSum {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            Integer value2 = map.get(nums[i]);
            if (value2 != null && value2!=i) {
                return new int[]{i, value2};
            }
            map.put(target - nums[i], i);
        }
        return null;
    }
}
