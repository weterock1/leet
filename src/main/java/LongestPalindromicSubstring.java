public class LongestPalindromicSubstring {

    public String longestPalindrome(String s) {
        if (s.length() == 1) {
            return s;
        }
        int endCur = s.length() - 1;
        int bestLength = 0;
        int bestStartPos = -1;
        int bestEndPos = -1;
        while (true) {
            for (int i = 0; i < endCur; i++) {
                if (endCur - i < bestLength) {
                    break;
                }
                if (i < endCur) {
                    boolean isPalyndrome = checkPalindom(i, endCur, s);
                    if (isPalyndrome) {
                        if (bestLength < endCur-i+1) {
                            bestLength = endCur-i+1;
                            bestStartPos = i;
                            bestEndPos = endCur;
                        }
                    }
                } else {
                    break;
                }
            }
            endCur--;
            if (endCur < 0 || bestLength > endCur) {
                break;
            }
        }
        if (bestLength == 0) {
            return s.substring(0,1);
        }
        return s.substring(bestStartPos, bestEndPos+1);
    }

    public String longestPalindrome2(String s) {
        if (s == null || s.length() < 1) {
            return "";
        }
        int start = 0, end = 0;
        for (int i = 0; i < s.length(); i++) {
            int len1 = expandAroundCenter(s, i, i);
            int len2 = expandAroundCenter(s, i, i + 1);
            int len = Math.max(len1, len2);
            if (len > end - start) {
                start = i - (len - 1) / 2;
                end = i + len / 2;
            }
        }
        return s.substring(start, end + 1);
    }

    private int expandAroundCenter(String s, int left, int right) {
        int L = left, R = right;
        while (L >= 0 && R < s.length() && s.charAt(L) == s.charAt(R)) {
            L--;
            R++;
        }
        return R - L - 1;
    }

    private boolean checkPalindom(int startCur, int endCur, String s) {
        while (startCur <= endCur) {
            if (s.charAt(startCur) == s.charAt(endCur)) {
                startCur++;
                endCur--;
            } else {
                return false;
            }
        }
        return true;
    }
}
