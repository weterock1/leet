import java.util.Arrays;

public class LongestSubstring {

    public int lengthOfLongestSubstring(String s) {

        int[] symbolsPosition = new int[128];
        Arrays.fill(symbolsPosition, -1);

        int bestLength = 0;
        int curLength = 0;
        for (int i = 0; i < s.length(); i++) {
            int symbol = symbolsPosition[s.charAt(i)];
            if (symbol == -1) {
                curLength++;
                symbolsPosition[s.charAt(i)] = i;
            } else {
                if (curLength > bestLength) {
                    bestLength = curLength;
                }
                curLength = i - symbolsPosition[s.charAt(i)];
                for (int i1 = 0; i1 < symbolsPosition.length; i1++) {
                    if (symbolsPosition[i1] < symbolsPosition[s.charAt(i)]) {
                        symbolsPosition[i1] = -1;
                    }
                }
                symbolsPosition[s.charAt(i)] = i;
            }
        }
        if (curLength > bestLength) {
            return curLength;
        }
        return bestLength;
    }

    public int lengthOfLongestSubstring2(String s) {
        int[] symbolsPosition = new int[128];
        int[] positionsSymbols = new int[s.length()];
        Arrays.fill(symbolsPosition, -1);
        int bestLength = 0;
        int curLength = 0;
        int prevCleaning = 0;
        for (int i = 0; i < s.length(); i++) {
            int symbol = symbolsPosition[s.charAt(i)];
            if (symbol == -1) {
                curLength++;
                symbolsPosition[s.charAt(i)] = i;
                positionsSymbols[i] = s.charAt(i);
            } else {
                if (curLength > bestLength) {
                    bestLength = curLength;
                }
                curLength = i - symbolsPosition[s.charAt(i)];
                for (int j = prevCleaning; j < symbolsPosition[s.charAt(i)]; j++) {
                    symbolsPosition[positionsSymbols[j]] = -1;
                }
                prevCleaning = symbolsPosition[s.charAt(i)] + 1;

                positionsSymbols[i] = s.charAt(i);
                symbolsPosition[s.charAt(i)] = i;
            }
        }
        if (curLength > bestLength) {
            return curLength;
        }
        return bestLength;
    }
}
